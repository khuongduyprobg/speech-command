### Module imports ###
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torchvision.transforms import transforms

import numpy as np
import librosa
import pickle
import PIL

from cnn_classifier import Classifier
import config as cf
from extract_mel_spectrogram import signal_to_melspectrogram, mono_to_color
from utils import Dataset_Loader
from test import load_model, preprocess


### Global Variables ###


### Class declarations ###


### Function declarations ###
def read_audio(wav_path):
    """
    Read audio from wav file
    Input: path of wav file
    Output: wav vector
    """
    orig_wav_vector, _sr = librosa.load(wav_path, sr=cf.sampling_rate)
    return orig_wav_vector


def embedding(model, orig_wav_vector):
    """
    Convert original wav vector to embedding vector
    Input: model, original_wav_vector
    Output: embedding vector
    """
    device = 'cuda:0' if torch.cuda.is_available else 'cpu'
    mels = signal_to_melspectrogram(orig_wav_vector)
    X_image = mono_to_color(mels)
    X_image = preprocess(X_image).view(1, 1, 128, -1).to(device)
    pred = model(X_image)
    return pred.detach().cpu().numpy()[0]


def embed_test_data(model):
    """
    Convert test set to embedding vectors
    """
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    with open(cf.test_mels, 'rb') as f:
        data = pickle.load(f)
    X, y = data[0], data[1]
    embed_vectors = []
    for i in range(len(X)):
        X_image = preprocess(X[i]).view(1, 1, 128, -1).to(device)
        embed_vectors.append(model(X_image).detach().cpu().numpy()[0])
    print(len(embed_vectors), embed_vectors[0])
    with open('data/embedded_mels.pkl', 'wb') as f:
        pickle.dump(embed_vectors, f)


if __name__ == '__main__':
    model = load_model('models/best_model.pkl', mode='embedding')
    embed_test_data(model)
