### Module imports ###
import librosa
import pandas as pd
import numpy as np
import pickle
import config as cf


### Global Variables ###
sequence_length = int(cf.duration * cf.sampling_rate)


### Class declarations ###


### Function declarations ###
def read_audio(audio_path):
    """
    Read audio and convert to vector
    Input: Wav_path
    Output: wav_vector
    """
    y, sr = librosa.load(audio_path, sr=cf.sampling_rate)
    # Remove zero length audio and cut silence
    if len(y) > 0:
        y, _ = librosa.effects.trim(y)
    else:
        print(f'find audio path with zero length')
    # Shape the same length audio
    if len(y) > sequence_length:
        # if trim_long_data:
        y = y[:sequence_length]
    else:
        padding = sequence_length - len(y)
        offset = padding // 2
        y = np.pad(y, (offset, sequence_length - len(y) - offset), 'constant')
    return y


def signal_to_melspectrogram(signal):
    """
    Convert wav vector to melspectrogram
    Input: wav_vector
    Output: melspectrogram
    """
    spectrogram = librosa.feature.melspectrogram(signal,
                                                 sr=cf.sampling_rate,
                                                 n_mels=cf.n_mels,
                                                 hop_length=cf.hop_length,
                                                 n_fft=cf.n_fft,
                                                 fmin=cf.fmin,
                                                 fmax=cf.fmax)
    spectrogram = librosa.power_to_db(spectrogram)
    spectrogram = spectrogram.astype(np.float32)
    return spectrogram


def mono_to_color(X, eps=1e-6):
    """ Stack X from 1 to 3 channels """
    # Stack X as [X, X, X]
    X = np.stack([X, X, X], axis=-1)

    # Standardize
    X = X - X.mean()
    Xstd = X / (X.std() + eps)
    _min, _max = Xstd.min(), Xstd.max()
    norm_max = _max
    norm_min = _min
    if (_max - _min) > eps:
        #Normalize to [0, 255]
        V = Xstd
        V[V < norm_min] = norm_min
        V[V > norm_max] = norm_max
        V = 255 * (V - norm_min) / (norm_max - norm_min)
        V = V.astype(np.uint8)
    else:
        # Just zero
        V = np.zeros_like(Xstd, dtype=np.uint8)
    return V


def save_to_binary(obj, filename):
    """ Save object to binary file """
    with open(filename, 'wb') as f:
        pickle.dump(obj, f)


def read_binary(filename):
    """ Read binary file """
    with open(filename, 'rb') as f:
        return pickle.load(f)


def save_features(input_path, output_path, mode):
    """
    Save features to files
    Input: input_path: csv_path
           output_path: output_path(*.pkl)
           mode: train - val - test
    Output: binary file created
    """
    df = pd.read_csv(input_path)
    audios = df['wav_file'].tolist()
    labels = df['label'].tolist()
    X, y = [], []
    for i in range(len(audios)):
        if mode == 'segment':
            orig_wav_vector, _sr = librosa.load(audios[i], sr=cf.sampling_rate)
            for index in range(0, orig_wav_vector.shape[0], sequence_length):
                end = min(orig_wav_vector.shape[0], index + sequence_length)
                segment = orig_wav_vector[index:end]
                if segment.shape[0] < sequence_length:
                    padding = sequence_length - segment.shape[0]
                    offset = padding // 2
                    segment = np.pad(segment, (offset, sequence_length - segment.shape[0] - offset), 'constant')
                mels = signal_to_melspectrogram(segment)
                X_image = mono_to_color(mels)
                X.append(X_image)
                y.append(labels[i])
        else:
            orig_wav_vector = read_audio(audios[i])
            mels = signal_to_melspectrogram(orig_wav_vector)
            X_image = mono_to_color(mels)
            X.append(X_image)
            y.append(labels[i])
    save_to_binary((X, y), output_path)


if __name__ == '__main__':
    save_features(cf.train_csv_v1, cf.train_mels_v1, mode='utt')
    save_features(cf.val_csv_v1, cf.val_mels_v1, mode='utt')
    save_features(cf.test_csv_v1, cf.test_mels_v1, mode='utt')

    save_features(cf.train_csv_v2, cf.train_mels_v2, mode='utt')
    save_features(cf.val_csv_v2, cf.val_mels_v2, mode='utt')
    save_features(cf.test_csv_v2, cf.test_mels_v2, mode='utt')

