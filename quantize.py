### Module imports ###
import torch
import torch.nn as nn
import torchvision
from torch.quantization import QuantStub, DeQuantStub
from torch.utils.data import DataLoader
from torchvision import datasets
import torchvision.transforms as transforms
import torch.quantization

import numpy as np
import os
import time
import sys
import pickle

import config as cf
from utils import Dataset_Loader
from test import load_model, test
import cnn_classifier
from train import prepare_data
from cnn_classifier import Classifier


### Global Variables ###
transforms_dict = {
    'train': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
    'test': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
}
train_loader = prepare_data('data_/train_mels.pkl', mode='train')
test_loader = prepare_data('data_/test_mels.pkl', mode='test')
model = load_model('models/best_model.pkl').to("cpu")
model.eval()
for m in model.modules():
    if type(m) == cnn_classifier.ConvBlock:
        m.fuse_block()
    if type(m) == torch.nn.modules.container.Sequential:
        if type(m[0]) == torch.nn.modules.Dropout and len(m) == 4:
            torch.quantization.fuse_modules(m, ['1', '2'], inplace=True)
criterion = nn.CrossEntropyLoss()


### Class declarations ###
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


### Function declarations ###
def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


def evaluate(model, criterion, data_loader, neval_batches):
    """ Compute top1, top5 accuracy """
    model.eval()
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    cnt = 0
    with torch.no_grad():
        for image, target in data_loader:
            output = model(image)
            loss = criterion(output, target)
            cnt += 1
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            print('.', end = '')
            top1.update(acc1[0], image.size(0))
            top5.update(acc5[0], image.size(0))
            if cnt >= neval_batches:
                 return top1, top5

    return top1, top5


def print_size_of_model(model):
    """ Compute size of model """
    torch.save(model.state_dict(), "temp.p")
    print('Size (MB):', os.path.getsize("temp.p")/1e6)
    os.remove('temp.p')


def load_model(model_file):
    """ Load model from model_path """
    model = Classifier(num_classes=30, mode="train")
    state_dict = torch.load(model_file)
    model.load_state_dict(state_dict)
    model.to('cpu')
    return model


if __name__ == '__main__':
    model.qconfig = torch.quantization.default_qconfig
    torch.quantization.prepare(model, inplace=True)
    num_calibration_batches = 10
    evaluate(model, criterion, train_loader, neval_batches=num_calibration_batches)

    torch.quantization.convert(model, inplace=True)
    top1, top5 = evaluate(model, criterion, test_loader, neval_batches=num_calibration_batches)
    print(top1.avg)
    model.fc2 = Identity()
    for m in model.modules():
        print(m)
    torch.jit.save(torch.jit.script(model), 'models/quantized.pth')
    for images, labels in train_loader:
        print(model(images), model(images).dtype)
    # print(model.parameters())
    # torch.save(model.state_dict(), 'models/quantized.pkl')
    # model.load_state_dict(torch.load('models/quantized.pkl'))
    # test(model, test_loader, device="cpu")
    # model = torch.jit.load('models/quantized_model.pkl')
    # print(model)
    exit()
    for m in model.modules():
        if type(m) == cnn_classifier.ConvBlock:
            m.fuse_block()
        if type(m) == torch.nn.modules.container.Sequential:
            if type(m[0]) == torch.nn.modules.Dropout and len(m) == 4:
                torch.quantization.fuse_modules(m, ['1', '2'], inplace=True)

