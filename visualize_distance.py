### Module imports ###
import numpy as np
import pandas as pd
import pickle
import os

from embedding import embedding, read_audio
from test import load_model
import config as cf


### Global variables ###


### Class declarations ###


### Function declarations ###
def euclide_distance(arr1, arr2):
    '''
    Input: 2 numpy array
    Output: euclide distance of 2 array
    '''
    return np.sqrt(np.sum(np.power((arr1 -arr2), 2)))


def cosine_similarity(arr1, arr2):
    '''
    Input: 2 numpy array
    Output: cosine similarity of 2 array
    '''
    dot = np.dot(arr1, arr2)
    norma = np.linalg.norm(arr1)
    normb = np.linalg.norm(arr2)
    cos = dot / (norma * normb)
    return cos


### Function declarations ###
if __name__ == '__main__':
    model = load_model('models/best_model.pkl', mode='embedding')
    data = 'data/Wake up/'
    wav = os.listdir(data)
    sample_0 = embedding(model, read_audio(os.path.join(data, wav[0])))
    pairs, euclide, cosine = [], [], []
    total = np.zeros(128,)
    for i in range(1, len(wav)):
        wav_path = os.path.join(data, wav[i])
        signal = read_audio(wav_path)
        vector = embedding(model, signal)
        total += vector
        pairs.append(f"Sample0 - Sample{i}")
        euclide.append(euclide_distance(sample_0, vector))
        cosine.append(cosine_similarity(sample_0, vector))
    print(pairs, euclide, cosine)

    mean = total / (len(wav) -1)
    pairs.append(f"Sample0 - Mean")
    euclide.append(euclide_distance(sample_0, mean))
    cosine.append(cosine_similarity(sample_0, mean))

    data_ = 'data/Sample'
    wav = os.listdir(data_)
    for i in range(len(wav)):
        wav_path = os.path.join(data_, wav[i])
        signal = read_audio(wav_path)
        vector = embedding(model, signal)
        print(vector)
        pairs.append(f"Sample0 - Other{i}")
        euclide.append(euclide_distance(sample_0, vector))
        cosine.append(cosine_similarity(sample_0, vector))

    print(pairs, euclide,cosine)

    df = pd.DataFrame(columns=['pair', 'euclide_distance', 'cosine_similarity'])
    df['pair'] = pairs
    df['euclide_distance'] = euclide
    df['cosine_similarity'] = cosine
    df.to_csv('visualize.csv', index=False)
