### Module imports ###
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torchvision.transforms import transforms

import numpy as np
import librosa
import pickle
import PIL

from cnn_classifier import Classifier
import config as cf
from extract_mel_spectrogram import signal_to_melspectrogram, mono_to_color
from utils import Dataset_Loader


### Global Variables ###
sequence_length = int(cf.duration * cf.sampling_rate)
transforms=transforms.Compose([transforms.RandomHorizontalFlip(0.5),
                                transforms.ToTensor()])


### Class declarations ###


### Function declarations ###
def load_model(model_path, mode='train', device="cuda:0"):
    """ Load model """
    # device = "cuda:0" if torch.cuda.is_available() else "cpu"
    model = Classifier(num_classes=30, mode=mode).to(device)
    model.load_state_dict(torch.load(model_path))
    model.eval()
    return model


def test(model, test_loader, device="cuda:0"):
    """ Test performance of model """
    # device = "cuda:0" if torch.cuda.is_available() else "cpu"
    correct, total = 0, 0
    with torch.no_grad():
        for data in test_loader:
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)
            outputs = model(inputs)
            _, predictions = torch.max(outputs.data, dim=1)
            total += labels.size(0)
            correct += (predictions == labels).sum().item()
        print(f"Accuracy: {correct / total}")


def preprocess(image):
    """ Preprocess from image """
    time_dim, base_dim = image.shape[1], image.shape[0]
    # crop = np.random.randint(0, time_dim - base_dim)
    # image = image[:, crop:crop + base_dim, ...]
    # freq_mask_begin = int(np.random.uniform(0, 1 - 0.1) * base_dim)
    freq_mask_begin = 2
    # image[freq_mask_begin:freq_mask_begin + int(0.1 * base_dim), ...] = 0
    # time_mask_begin = int(np.random.uniform(0, 1 - 0.1) * base_dim)
    time_mask_begin = 1
    # image[:, time_mask_begin:time_mask_begin + int(0.1 * base_dim), ...] = 0
    image = PIL.Image.fromarray(image[...,0], mode='L')
    image = transforms(image).div_(255)
    return image


def predict(pred):
    """ Predict class of sample """
    with open('data/command_dict.pkl', 'rb') as f:
        class_dict = pickle.load(f)
    index = torch.argmax(pred, dim=1)
    for key in class_dict.keys():
        if class_dict[key] == index:
            return key


def inference(model, wav_path):
    """Infer from wav_file """
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    orig_wav_vector, _sr = librosa.load(wav_path, sr=cf.sampling_rate)
    for index in range(0, orig_wav_vector.shape[0], sequence_length):
        end = min(orig_wav_vector.shape[0], index + sequence_length)
        segment = orig_wav_vector[index:end]
        if segment.shape[0] < sequence_length:
            padding = sequence_length - segment.shape[0]
            offset = padding // 2
            segment = np.pad(segment, (offset, sequence_length - segment.shape[0] - offset), 'constant')
        mels = signal_to_melspectrogram(segment)
        X_image = mono_to_color(mels)
        X_image = preprocess(X_image).view(1, 1, 128, -1).to(device)
        pred = model(X_image)
        result = predict(pred)
        print(result)


if __name__ == '__main__':
    '''with open(cf.test_mels, 'rb') as f:
        data = pickle.load(f)
    X, y = data[0], data[1]
    test_dataset = Dataset_Loader(X, y, transforms=transforms)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=64, shuffle=True)

    model = load_model('models/quantized_model.pkl', device="cpu")

    test(model, test_loader, device="cpu")
    '''
    sample = np.random.randn(3, 128, 128)
    preprocess(sample)
    # inference(model, 'data/on/06076b6b_nohash_2.wav')
