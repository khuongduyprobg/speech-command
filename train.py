### Module imports ###
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torchvision.transforms import transforms

import numpy as np
import pandas as pd
import argparse
import pickle
import os
from fastprogress import master_bar, progress_bar

from cnn_classifier import Classifier
import config as cf
from extract_mel_spectrogram import read_binary
from utils import Dataset_Loader


### Global Variables ###
transforms_dict = {
    'train': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
    'test': transforms.Compose([
        transforms.RandomHorizontalFlip(0.5),
        transforms.ToTensor(),
    ]),
}


### Class declarations


### Function declarations
def prepare_data(input_path, mode='train'):
    """ From features to dataloader """
    with open(input_path, 'rb') as f:
        temp = pickle.load(f)
    X, y = temp[0], temp[1]
    dataset = Dataset_Loader(X, y, transforms=transforms_dict[mode])
    # X = torch.tensor(X, dtype=torch.float32)
    # y = torch.tensor(y, dtype=torch.long)
    # X = torch.from_numpy(X)
    # y = torch.from_numpy(y)
    # data = torch.utils.data.TensorDataset(X, y)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=16, shuffle=True)
    return dataloader


def train_model(args):
    """ Train model and evaluate """
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    model = Classifier(num_classes=30, mode='train').to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(params=model.parameters(), lr=args.lr, amsgrad=False)
    performance = 0
    mb = master_bar(range(args.epochs))

    for epoch in mb:
        print(f"Training epoch:{epoch}")
        model.train()
        for inputs, labels in progress_bar(train_loader, parent=mb):
            # inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)

            model.zero_grad()
            optimizer.zero_grad()

            predictions = model(inputs)
            predictions = predictions.to(device)

            loss = criterion(predictions, labels)
            loss.backward()
            optimizer.step()
        # Evaluate
        correct = 0
        total = 0
        with torch.no_grad():
            for data in val_loader:
                inputs, labels = data
                inputs = inputs.to(device)
                labels = labels.to(device)
                outputs = model(inputs)
                _, predictions = torch.max(outputs.data, dim=1)
                total += labels.size(0)
                correct += (predictions == labels).sum().item()
            print(f"Accuracy: {correct / total}")
            if correct / total > performance:
                performance = correct / total
                if not os.path.exists(cf.models_path):
                    os.makedirs(cf.models_path)
                torch.save(model.state_dict(), 'models/best_model.pkl')
            print(f"Best accuracy: {performance}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=1000)
    parser.add_argument('--lr', type=float, default=1e-6)
    args = parser.parse_args()
    prepare_data(cf.train_mels, mode='train')

    train_loader = prepare_data(cf.train_mels, mode='train')
    val_loader = prepare_data(cf.val_mels, mode='test')
    test_loader = prepare_data(cf.test_mels, mode='test')

    train_model(args)
