### Module imports ###
import os
import pandas as pd
import pickle
import argparse

import config as cf


### Global Variables ###
overlap = []  # List of overlap files in dev/test set in audios folder


### Class declarations ###


### Function declarations ###
def create_test(path, return_mode='no_dict'):
    """
    Create database for dev/test set
    Input: txt file of dev/test set
    Output: wav_path list, label list, command dictionary of labels(optinal)
    """
    wav, label = [], []
    command_dict, command_num = {}, 0
    with open(path, 'r') as f:
        for line in f:
            overlap.append(os.path.join(data_dir, line[:-1]))
            wav.append(os.path.join(data_dir, line[:-1]))
            label.append(line.split('/')[0])
            if line.split('/')[0] not in command_dict:
                command_dict[line.split('/')[0]] = command_num
                command_num += 1
    if return_mode == 'no_dict':
        return wav, label
    return wav, label, command_dict


def create_df(data, label, command_dict, output):
    """
    Create csv file for train/dev/test set
    Input: data: list of wav file path
           label: list of labels
           command_dict: Dictionary for labels
           output: csv output path
    Output: create csv files
    """
    df = pd.DataFrame(columns= ['wav_file', 'label'])
    df['wav_file'] = data
    df['label'] = label
    df['label'] = df['label'].map(command_dict)
    df = df.sort_values(by=['label'])
    df.to_csv(output, index = False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ver', type=str, default='1')
    args = parser.parse_args()

    data_dir = cf.data_dir_v1 if args.ver == '1' else cf.data_dir_v2
    test_data = cf.test_data_v1 if args.ver == '1' else cf.test_data_v2
    val_data = cf.val_data_v1 if args.ver == '1' else cf.val_data_v2
    train_csv = cf.train_csv_v1 if args.ver == '1' else cf.train_csv_v2
    val_csv = cf.val_csv_v1 if args.ver == '1' else cf.val_csv_v2
    test_csv = cf.test_csv_v1 if args.ver == '1' else cf.test_csv_v2
    command_dict_path = cf.command_dict_v1 if args.ver == '1' else cf.command_dict_v2

    wav_test, label_test = create_test(test_data)
    wav_val, label_val, command_dict = create_test(val_data, return_mode='dict')
    folder = os.listdir(data_dir)
    wav_train, label_train = [], []
    for fn in folder:
        if not fn.endswith('txt') and not fn.endswith('_'):
            folder_path = os.path.join(data_dir, fn)
            wav_list = os.listdir(folder_path)
            for wav in wav_list:
                path = os.path.join(folder_path, wav)
                if path not in overlap:
                    wav_train.append(path)
                    label_train.append(fn)
    create_df(wav_train, label_train, command_dict, train_csv)
    create_df(wav_val, label_val, command_dict, val_csv)
    create_df(wav_test, label_test, command_dict, test_csv)

    with open(command_dict_path, 'wb') as f:
        pickle.dump(command_dict, f)
