data_dir_v1 = 'speech_commands_v0.01/'
test_data_v1 = 'speech_commands_v0.01/testing_list.txt'
val_data_v1 = 'speech_commands_v0.01/validation_list.txt'

data_dir_v2 = 'speech_commands_v0.02/'
test_data_v2 = 'speech_commands_v0.02/testing_list.txt'
val_data_v2 = 'speech_commands_v0.02/validation_list.txt'

train_csv_v1 = 'data/v1/audio_train.csv'
val_csv_v1 = 'data/v1/audio_val.csv'
test_csv_v1 = 'data/v1/audio_test.csv'

train_csv_v2 = 'data/v2/audio_train.csv'
val_csv_v2 = 'data/v2/audio_val.csv'
test_csv_v2 = 'data/v2/audio_test.csv'

train_mels_v1 = 'data/v1/train_mels.pkl'
val_mels_v1 = 'data/v1/val_mels.pkl'
test_mels_v1 = 'data/v1/test_mels.pkl'

train_mels_v2 = 'data/v2/train_mels.pkl'
val_mels_v2 = 'data/v2/val_mels.pkl'
test_mels_v2 = 'data/v2/test_mels.pkl'

command_dict_v1 = 'data/v1/command_dict.pkl'
command_dict_v2 = 'data/v2/command_dict.pkl'

models_path = 'models'

sampling_rate = 16000
duration = 1
n_mels = 128
hop_length = int(125 * duration)
fmin = 20
fmax = sampling_rate // 2
n_mels = 128
n_fft = n_mels * 20

